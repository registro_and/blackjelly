[![BlackJelly.js](https://i.imgur.com/GI867ec.png)](https://bitbucket.org/registro_and/blackjelly/src/master/)

BlackJelly es una herramienta para ayudarte a planificar, organizar y gestionar el trabajo de tu equipo, de principio a fin. Un software de gestión de proyectos que también actúa como herramienta de colaboración. Puedes coordinar las tareas del equipo para que todos sepan quién hace qué. Comparte comentarios, archivos y actualizaciones de estado. Además, obtén una vista completa del trabajo para que los equipos realicen las tareas correctas en el momento correcto y a nivel especifico y gerencial.


## Inicio

En este apartado se explica cómo se puede comenzar a trabajar con el servicio de BlackJelly y las herramientas de las que depende.

Para una mayor información, por favor consulta: https://BlackJelly.run/docs

###Requisitos Previos

Este Servicio y componentes está desarrollado en [BlackOcean.js](https://BlackOcean.run) y Javascript ES6 (o superior), con [Node.js](https://nodejs.org/es/) [v8.11.2](https://nodejs.org/dist/v8.11.2/node-v8.11.2.pkg), por lo que se requerirá tener instalada una versión de Node.js igual o superior.

Este servicio depende expresamente del Framework [BlackOcean.js](https://BlackOcean.run), por lo que se requiere instalar tal como se indica [aqui](https://www.npmjs.com/package/black-ocean).

### Instalación/Ejecución 

...


## Autores

* Jesús E. Aldréte Hernández<br> <jaldrete@novasolutionsystems.com>

## License

Este proyecto está licenciado bajo la Licencia MIT - Véase el archivo [LICENSE](https://bitbucket.org/registro_and/blackjelly/src/master/LICENSE.md) para más detalles.